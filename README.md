Modèle pour la génération de supports de cours (poly+slides) au format html, pdf et même docx à partir d'une source unique (au format markdown/pandooc).

Une installation *récente* de  pandoc est nécessaire.

Comment ça fonctionne:

- Les fichiers source sont à mettre dans le répertoire `sources`.
- Les figures au format `svg` sont à mettre dans le répertoire `svgs`
- Dans le `Makefile`, modifier la variable `FILES`. Elle doit contenir la liste des fichiers à compiler.
- puis `make`

Les répertoires suivant sont alors créés:

- `web` : les document compilés au format html
- `pdfs` : les document compilés au format pdf
- `web_slides` : les transparents compilés au format html (reveal.js)
- `pdf_slides` :  les transparents compilés au format pdf (beamer)
- `gen_pdf` : les figures svg converties en pdf pour être utilisées par Latex
- `gen_png` :  les figures svg converties en png pour être utilisées dans les docx

Si vous voulez générer un docx, il faut explicitement appeler la commande `make docx`.
Le résultat est généré dans le dossier docs.

**TODO:** il y a un problème avec les svgs dans les docx quand les markdown sont inclus dans un autre markdown.

### Les macros prédéfinies:

En plus de `pandoc` des script lua (exécuté par l'interpréteur intégré de pandoc) permettent de remplacer un préprocesseur.

Deux macros sont définies:

- les images au format svg sont remplacée par des pdf (générés par le makefile) quand la cible est un pdf (utilisant latex) (*voir utils.lua*)
- les blocs `:::{.if HANDOUT=true} ... :::` n'apparaissent que si la la variable `HANDOUT` est définie (*voir panda.lua et le makefile*)
