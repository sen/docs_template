SVG_SRC = $(wildcard svgs/*.svg)
SVG_PDF = $(patsubst svgs/%.svg,gen_pdf/%.pdf,$(SVG_SRC))
SVG_PNG = $(patsubst svgs/%.svg,gen_png/%.png,$(SVG_SRC))

.PHONY: clean_graphics

GRAPHICS = $(SVG_PDF) $(SVG_PNG)

graphics: $(GRAPHICS)

clean_graphics:
	rm -fr gen_pdf
	rm -fr gen_png

gen_pdf/%.pdf:svgs/%.svg | gen_pdf
	inkscape --export-text-to-path --export-type=pdf $< -o $@

gen_png/%.png:svgs/%.svg | gen_png
	inkscape --export-text-to-path --export-type=png $< -o $@

gen_pdf:
	[ -d $@ ] || mkdir $@

gen_png:
	[ -d $@ ] || mkdir $@

