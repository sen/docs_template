---
lang      : fr
title     : BlaBla Cours
subtitle  : Number One
author    : John Doe
date      : P3 2021-2226
institute : "Telecom Paris"
---

# Premier Chapitre

:::{include=sources/part1.md shift=1}
Remplacé par la section à avec un niveau de titre plus bas
:::

:::{include=sources/part2.md shift=1}
Remplacé par la section à avec un niveau de titre plus bas
:::

# Second Chapitre

:::{include=sources/part1.md shift=1}
Remplacé par la section à avec un niveau de titre plus bas
:::

:::{include=sources/part2.md shift=1}
Remplacé par la section à avec un niveau de titre plus bas
:::
