FILES        = foo

PDF_DIR     = pdfs
BEAMER_DIR  = pdf_slides
DOC_DIR     = docs
HTML_DIR    = web
HTMLS_DIR   = web_slides

PDF_DOCS    = $(patsubst %,$(PDF_DIR)/%.pdf,    $(FILES))
PDF_SLIDES  = $(patsubst %,$(BEAMER_DIR)/%.pdf, $(FILES))
DOCX_DOCS   = $(patsubst %,$(DOC_DIR)/%.docx,   $(FILES))

HTML_PAGES  = $(patsubst %,$(HTML_DIR)/%.html,  $(FILES))
HTML_SLIDES = $(patsubst %,$(HTMLS_DIR)/%.html, $(FILES))

PANDOCOPTS  = -f markdown
PANDOCOPTS += -L lua/panda.lua
PANDOCOPTS += -L lua/utils.lua
PANDOCOPTS += -L lua/pagebreak.lua

.PHONY: beamer hslides pdf html all clean

VPATH = sources

include graphics.mk

.DEFAULT_GOAL := all

all: graphics
	make -j4 html
	make -j4 hslides
	make -j4 pdf
	make -j4 beamer
	make -j4 docx

pdf     : $(PDF_DOCS)
beamer  : $(PDF_SLIDES)
html    : $(HTML_PAGES)
hslides : $(HTML_SLIDES)
docx    : $(DOCX_DOCS)


$(PDF_DIR)/%.pdf:%.pdf | $(PDF_DIR)
	mv $< $@

$(BEAMER_DIR)/%.pdf:%.pdf | $(BEAMER_DIR)
	mv $< $@

$(DOC_DIR)/%.docx:%.docx | $(DOC_DIR)
	mv $< $@

$(HTML_DIR)/%.html:%.html| $(HTML_DIR)
	mv $< $@

$(HTMLS_DIR)/%.html:%.html| $(HTMLS_DIR)
	mv $< $@

# Generic rules
%.html:%.md
	pandoc $(PANDOCOPTS) $< -o $@

%.pdf:%.md
	pandoc $(PANDOCOPTS) $< -o $@

%.docx:%.md
	pandoc $(PANDOCOPTS) $< -o $@

# Global latex options
%.pdf:  PANDOCOPTS += --pdf-engine=lualatex
# Global HTML options
%.html: PANDOCOPTS += --mathjax

# Single HTML file
ifeq ($(MAKECMDGOALS),html)
PANDOCOPTS += --toc
PANDOCOPTS += -c ../css/pandoc.css
PANDOCOPTS += -t html
PANDOCOPTS += -A templates/footer.html
PANDOCOPTS += -M HANDOUT=true
endif

# Single PDF File
ifeq ($(MAKECMDGOALS),pdf)
PANDOCOPTS += --toc
PANDOCOPTS += -t latex
PANDOCOPTS += options/pdf.yaml
PANDOCOPTS += --template=templates/tpt.latex
PANDOCOPTS += -M HANDOUT=true
endif

# Beamer presentation
ifeq ($(MAKECMDGOALS),beamer)
PANDOCOPTS += --toc
PANDOCOPTS += --slide-level 2
PANDOCOPTS += -t beamer
PANDOCOPTS += options/beamer.yaml
PANDOCOPTS += --template=templates/tpt.beamer
endif

# HTML presentation
ifeq ($(MAKECMDGOALS),hslides)
PANDOCOPTS += -s
PANDOCOPTS += --slide-level 2
#PANDOCOPTS += -i
PANDOCOPTS += -t revealjs
PANDOCOPTS += -V revealjs-url=./reveal.js
PANDOCOPTS += options/reveal.yaml
endif

# Single DOCX file
ifeq ($(MAKECMDGOALS),docx)
PANDOCOPTS += -t docx
PANDOCOPTS += --toc
PANDOCOPTS += -M HANDOUT=true
endif


clean: clean_graphics
	rm -rf $(PDF_DIR)
	rm -rf $(BEAMER_DIR)
	rm -rf $(HTML_DIR)
	rm -rf $(HTMLS_DIR)
	rm -rf $(DOC_DIR)

$(PDF_DIR):
	[ -d $@ ] || mkdir $@
$(BEAMER_DIR):
	[ -d $@ ] || mkdir $@
$(DOC_DIR):
	[ -d $@ ] || mkdir $@
$(HTML_DIR):
	[ -d $@ ] || mkdir $@
	ln -s ../svgs $@/svgs
$(HTMLS_DIR):
	[ -d $@ ] || mkdir $@
	ln -s ../svgs $@/svgs
	ln -s ../reveal.js $@/reveal.js
