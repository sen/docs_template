-- Replace svg whith generated pdf/png depending on the output format

if FORMAT:match 'beamer' or FORMAT:match 'latex' or FORMAT:match 'docx' then
  function Image(elem)

    local function file_exists(name)
      local f = io.open(name, 'r')
      if f then
        f:close()
        return true
      else
        return false
      end
    end

    local function subst_img_file(name)
      local bname = name:match 'svgs/(.*)%.svg'
      -- if we have an svg file
      if  bname then
        local sub_fname
        -- replace with pdf for latex
        if FORMAT:match 'beamer' or FORMAT:match 'latex' then
          sub_fname = "gen_pdf/" .. bname .. ".pdf"
          -- replace with png for docx
        elseif FORMAT:match 'docx' then
          sub_fname = "gen_png/" .. bname .. ".png"
        end
        -- check if the replacement file exist
        if sub_fname and file_exists(sub_fname) then
          name = sub_fname
        end
      end
      -- return the replacement file or the orginal file name
      -- if no replacement was found
      return name
    end

    elem.src = subst_img_file(elem.src)

    return elem
  end
end
